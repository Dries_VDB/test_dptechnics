#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>

/*To start this server first compile the program like gcc -o Server server.c
Then To start ./Server (ip adress) (Port number) like ./Server 127.0.0.1 5860*/

/*
This function belowe is called when a system cal fails 
*/
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

void *SigCatcher(int n)
{
  wait3(NULL,WNOHANG,NULL);
}

int main(int argc, char *argv[])
{
    /* sockfd and newsockfd are file descriptors, i.e. array subscripts into the file descriptor table . 
    These two variables store the values returned by the socket system call and the accept system call.
    portno is were the port number wil be stored for de socket */
    int sockfd, newsockfd, portno, pid;
    socklen_t clilen;

    /*sockaddr_in is a structure containing an internet address. This structure is defined in netinet/in.h.*/
    struct sockaddr_in serv_addr, cli_addr;

    /* Checking if there are enof argument given to start the server */
    if (argc < 2) {
        fprintf(stderr,"ERROR, no port provided \n");
        exit(1);
    }
    /* The socket() system call creates a new socket. It takes three arguments. 
     The first is the address domain of the socket.
     The second argument is the type of socket.
     The third argument is the protocol.*/
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
         error("ERROR opening socket");
    }   

    /*The function bzero() sets all values in a buffer to zero. It takes two arguments, 
    the first is a pointer to the buffer and the second is the size of the buffer. 
    Thus, this line initializes serv_addr to zeros. ----*/  
    bzero((char *) &serv_addr, sizeof(serv_addr));

    /* Setting de variables for the server to the correct valeus*/
    portno = (int) strtol(argv[2], (char **)NULL, 10);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    /*The bind() system call binds a socket to an address, 
    in this case the address of the current host and port number on which the server will run.
    It takes three arguments, the socket file descriptor, 
    the address to which is bound, and the size of the address to which it is bound.*/
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
        error("ERROR on binding");
    } 

    /* The listen system call allows the process to listen on the socket for connections. 
    The first argument is the socket file descriptor, and the second is the size of the backlog queue, i.e., 
    the number of connections that can be waiting while the process is handling a particular connection.*/       
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
    signal(SIGCHLD,SIG_IGN);
    
    while (1) {
    /*The accept() system call causes the process to block until a client connects to the server. 
    Thus, it wakes up the process when a connection from a client has been successfully established.*/
        newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0){
            error("ERROR on accept\r\n");
        }
        /* The fork function creates a exacte copy of the calling procces*/       
        pid = fork();
        if (pid < 0)
            error("ERROR on fork\r\n");
        if (pid == 0)  {
            close(sockfd);
             /*Hier komt het terug sturen van de header maar de juiste functie heb ik nog niet gevonden*/
            //recv(newsockfd,)
            
            exit(0);
        }
        else close(newsockfd);
    } 
    close(sockfd);
    return 0; 
}

